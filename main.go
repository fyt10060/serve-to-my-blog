package main

import (
	"fmt"

	"github.com/valyala/fasthttp"
)

func Handler(ctx *fasthttp.RequestCtx) {
	ctx.WriteString("Hello world!")
	fmt.Println("a")
}

func main() {

	fasthttp.ListenAndServe(":443", Handler)
}
